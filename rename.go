package rename

import (
	"fmt"

	"github.com/kpango/glg"
	"github.com/stmcore/go-sshclient"
)

func Rename(host, username, password, sourcePath, destPath string) error {

	client, err := sshclient.DialWithPasswd(host+":22", username, password)
	if err != nil {
		return err
	}
	defer client.Close()
	//"mv " + sourcePath + " " + destPath

	glg.Info("rename at:", host, sourcePath, "->", destPath)
	command := fmt.Sprintf("sudo mv -f \"%s\" \"%s\"", sourcePath, destPath)
	out, err := client.Cmd(command).SmartOutput()
	if err != nil {
		return err
	}
	fmt.Println(string(out))

	return nil

	// inputFile, err := os.Open(sourcePath)
	// if err != nil {
	// 	return fmt.Errorf("Couldn't open source file: %s", err)
	// }
	// outputFile, err := os.Create(destPath)
	// if err != nil {
	// 	inputFile.Close()
	// 	return fmt.Errorf("Couldn't open dest file: %s", err)
	// }
	// defer outputFile.Close()
	// _, err = io.Copy(outputFile, inputFile)
	// inputFile.Close()
	// if err != nil {
	// 	return fmt.Errorf("Writing to output file failed: %s", err)
	// }
	// // The copy was successful, so now delete the original file
	// err = os.Remove(sourcePath)
	// if err != nil {
	// 	return fmt.Errorf("Failed removing original file: %s", err)
	// }
	// return nil

}
